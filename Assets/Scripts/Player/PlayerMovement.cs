﻿using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    // Animation speed
    public float walkSpeed = 1f;

    private CharacterController controller;
    private Animator anim;
    private int floorMask;


    void Awake()
    {
        controller = GetComponent<CharacterController>();
        floorMask  = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();

        anim.speed = walkSpeed;
    }

    void Update()
    {
        // Determine new movement direction
        Vector3 movement = new Vector3(
            Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")
        );

        // Fix movement direction
        // This should match the camera angle
        //movement = Quaternion.Euler(0, -30, 0) * movement;
        movement = movement.normalized;

        Quaternion lookDirection = getLookDirection();

        //Move(movement);
        Turn(lookDirection);
        Animate(movement, lookDirection);
    }

    /// <summary>
    /// Apply movement
    /// </summary>
    void Move(Vector3 movement)
    {
        Vector3 velocity = movement * walkSpeed;

        // Gravity simulation
        if (controller.isGrounded)
        {
            // Player has landed, reset vertical velocity
            velocity.y = 0;
        } else
        {
            // Player is in air, apply gravity
            velocity.y += Physics.gravity.y;
        }

        // Finally, apply movement
        controller.Move(velocity * Time.deltaTime);
    }

    /// <summary>
    /// Apply turning
    /// </summary>
    void Turn(Quaternion newRotation)
    {
        transform.rotation = newRotation;
    }

    Quaternion getLookDirection()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(camRay, out hit, 1000f, floorMask))
        {
            Vector3 playerToMouse = hit.point - transform.position;
            playerToMouse.y = 0;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            return newRotation;
        }

        return Quaternion.identity;
    }


    /// <summary>
    /// Apply animations
    /// </summary>
    void Animate(Vector3 walkDirection, Quaternion lookDirection)
    {
        // TODO: Fix rotation, so movement happens in "world" sense
        Vector3 movement = walkDirection;

        // Blend animations
        anim.SetFloat("HorizontalBlend", movement.x);
        anim.SetFloat("VerticalBlend", movement.z);
    }

    void FootR()
    {
        //Debug.Log("Right foot");
    }

    void FootL()
    {
        //Debug.Log("Left foot");
    }
}
