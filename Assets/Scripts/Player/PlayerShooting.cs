﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 0649 

public class PlayerShooting : MonoBehaviour {

    public float firerate = 0.5f;
    public float range    = 100f;
    public int damage     = 20;

    float timer;
    Ray shootRay;
    RaycastHit shootHit;

    // Visuals
    LineRenderer shootLine;
    Light shootLight;
    AudioSource shootAudio;
    float effectDisplayTime = 0.5f;

    int shootableMask;

    Transform headBone;

    // Use this for initialization
    void Start ()
    {
        shootableMask = LayerMask.GetMask("Shootable");

        // Indexes vary between 
        headBone = GetComponentInParent<SkinnedMeshRenderer>().bones[1].transform;

        // Get references
        shootLine  = GetComponent<LineRenderer>();
        shootLight = GetComponent<Light>();
        shootAudio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;

        if (Input.GetButton("Fire1") && timer >= firerate)
        {
            Shoot();
        }

        if (timer >= firerate * effectDisplayTime)
        {
            ShowGunEffects(false);
        }

        shootLine.SetPosition(0, shootRay.origin);
    }

    void Shoot()
    {
        timer = 0f;

        // Set some other positions
        shootLine.transform.position = headBone.position;
        

        shootRay.origin    = transform.position;
        shootRay.direction = transform.forward;

        // Enable visuals
        ShowGunEffects(true);
        PlayGunAudio();


        shootLine.SetPosition(0, shootRay.origin);
        // Try to hit something
        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            shootLine.SetPosition(1, shootHit.point);

            // Get enemy that we hit
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();

            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damage, shootHit.point);
            }
        } else
        {
            shootLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }
    }

    void ShowGunEffects(bool state)
    {
        shootLine.enabled  = state;
        shootLight.enabled = state;
    }

    void PlayGunAudio()
    {
        shootAudio.pitch *= Random.Range(0.9f, 1.4f);
        shootAudio.Play();
        shootAudio.pitch = 1; // Reset pitch
    }
}
