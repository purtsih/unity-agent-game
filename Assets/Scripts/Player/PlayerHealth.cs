﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public int health = 100;
    public Slider healthSlider;

    private PlayerMovement movementScript;
    private PlayerShooting shootingScript;
    private Animator anim;

    public void TakeDamage(int damage)
    {
        health -= damage;
        healthSlider.value = health;

        if (health <= 0)
        {
            Death();
        }
    }

    void Start()
    {
        anim = GetComponent<Animator>();

        // Set health defaults
        healthSlider.maxValue = health;
        healthSlider.value    = health;

        movementScript = GetComponent<PlayerMovement>();
        //shootingScript = GetComponent<PlayerShooting>();
    }

    // Player dies
    void Death()
    {
        // Disable player movement and shooting
        movementScript.enabled = false;
        //shootingScript.enabled = false;

        // Play death animation
        anim.speed = 1; // Movement usually has some higher speed multiplier
        anim.SetTrigger("Death");
    }
}
