﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour {

    public TextMeshProUGUI scoreText;

    static int score;

	void Start ()
    {
        score = 0;
	}
	
    void Update()
    {
        scoreText.SetText("Score: " + score);
    }

    // A way for other scripts to add score
	public static void AddScore(int amount)
    {
        score += amount;
    }
}
