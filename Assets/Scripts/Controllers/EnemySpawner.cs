﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour {

    public GameObject enemyToSpawn;
    public PlayerHealth playerHealth;
    public float spawnTime = 5f;
    public Transform[] spawnPoints;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
	}

    void Spawn()
    {
        // Don't spawn enemies if player is dead
        if (playerHealth.health <= 0)
        {
            return;
        }

        int spawn = Random.Range(0, spawnPoints.Length);

        var spawned = Instantiate(enemyToSpawn, spawnPoints[spawn].position, spawnPoints[spawn].rotation);

        // Randomize enemy stats
        spawned.GetComponent<NavMeshAgent>().speed         *= Random.Range(0.7f, 1.5f);
        spawned.GetComponent<EnemyHealth>().startingHealth += 10 * Random.Range(-1, 5);
        spawned.GetComponent<EnemyAttack>().attackDamage   += Random.Range(-5, 10);
    }
}
