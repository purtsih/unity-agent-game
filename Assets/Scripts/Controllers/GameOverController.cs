﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {

    public PlayerHealth playerHealth;
    public float restartDelay = 5f;
    public Animator gameoverAnim;

    float restartTimer;

	// Update is called once per frame
	void Update () {
		if (playerHealth.health <= 0)
        {
            gameoverAnim.SetTrigger("GameOver");

            restartTimer += Time.deltaTime;

            if (restartTimer >= restartDelay)
            {
                // Reload the level 
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
	}
}
