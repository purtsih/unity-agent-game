﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


    public float followSpeed = 5f;
    public Transform followTarget;
	
    // Initialization offset from the follow target
    private Vector3 offset;

    void Start()
    {
        offset = transform.position - followTarget.position;
    }

	// Update is called once per frame
	void LateUpdate ()
    {
        transform.position = followTarget.position + offset;
	}
}
