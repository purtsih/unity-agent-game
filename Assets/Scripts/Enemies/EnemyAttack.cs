﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

    GameObject player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    bool playerInRange;
    float syncTimer;

	// Use this for initialization
	void Start () {
        player       = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth  = GetComponent<EnemyHealth>();
    }

    void OnTriggerEnter(Collider other)
    {
        // Check if player enters the attack range
        if (other.gameObject == player)
        {
            playerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Check if player leaves the range
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
    }

	// Update is called once per frame
	void Update () {
        syncTimer += Time.deltaTime;

        if (syncTimer > timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
        {
            Attack();
        }
	}

    void Attack()
    {
        // Attack the player
        syncTimer = 0f;

        if (playerHealth.health > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
