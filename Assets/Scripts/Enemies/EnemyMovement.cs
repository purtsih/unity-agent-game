﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {

    Transform player;
    NavMeshAgent agent;

    EnemyHealth health;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        agent  = GetComponent<NavMeshAgent>();
        health = GetComponent<EnemyHealth>();
	}
	
	// Update is called once per frame
	void Update () {

        if (health.currentHealth > 0)
        {
            agent.SetDestination(player.position);
        }
	}
}
