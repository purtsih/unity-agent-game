﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHealth : MonoBehaviour {

    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;

    bool isDead = false;
    bool isSinking = false;

    ParticleSystem hitParticles;

	// Use this for initialization
	void Start () {
        currentHealth = startingHealth;
        hitParticles = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        // Crude death animation, sink enemies through the floor
        if (isSinking)
        {
            transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }

    public void TakeDamage(int amount, Vector3 hitpoint)
    {
        if (isDead)
        {
            return;
        }

        currentHealth -= amount;

        /*var emitParams = new ParticleSystem.EmitParams();
        emitParams.position = hitpoint;

        hitParticles.Emit(emitParams, 5);*/
        hitParticles.Play();

        if (currentHealth <= 0)
        {
            isDead = true;
            ScoreManager.AddScore(scoreValue);
            StartSinking();
        }
    }

    void StartSinking()
    {
        GetComponent<NavMeshAgent>().enabled  = false;
        GetComponent<Rigidbody>().isKinematic = true;

        isSinking = true;

        // Destroy self after 2 seconds
        Destroy(gameObject, 2f);
    }
}
